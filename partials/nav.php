<div class="container-fluid px-0 sticky-top">
    <div class="header-top">
        <form class="form-inline my-3 my-lg-0">
                        <div class="top-user">
                            <a href="login.php"><i class="fas fa-user user-logo"></i></a>

                    <?php if(isset($_SESSION['logged_in_user'])) : ?>
                            <span class="greeting">Welcome, <?= $_SESSION['logged_in_user']; ?> | </span>
                            <a href="logout.php" style="text-decoration: none; display: inline;" class="logout">Logout</a>

                    <?php endif; ?>
               </div>


                <!-- <div class="top-user">
                    <label for="user"><i class="fas fa-user user-logo"></i></label>
                    <input class="form-control form-control-xs mr-sm-1 search-input" type="text" id="user">
                </div> -->

                <div class="top-search">
                    <input class="form-control form-control-xs mr-sm-1 search-input" type="search" id="search">
                     <label for="search"><i class="fas fa-search search-logo"></i></label>
                </div>
        </form>
    </div> <!-- end of HEADER-TOP -->

<?php if(isset($_SESSION['role']) && $_SESSION['role']=='admin') {
   get_admin_nav();
  } else {get_user_nav();}  ?>

<?php

function get_user_nav() { ?>

    <nav class="navbar navbar-expand-lg navbar-light" id="nav-bg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav ml-auto mr-auto mt-2 mt-lg-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="shop.php">Shop</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" href="contact.php">Contact Us</a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i>Cart</a>
                </li>
            </ul>
        </div>
    </nav>
<?php } ?>

<?php

function get_admin_nav() { ?>

    <nav class="navbar navbar-expand-lg navbar-light" id="nav-bg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav ml-auto mr-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="add_items_admin.php">Add Products</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_dashboard.php">View Product List</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin_orders.php">View Orders</a>
                </li>
            </ul>
        </div>
    </nav>

<?php } ?>
</div>