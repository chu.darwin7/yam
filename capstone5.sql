
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` decimal(13,2) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `type_name` varchar(255),
  `subcategory_id` int(11)
);


CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type_name` varchar(255),
  `category_id` int(11)
);

ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_fk0` (`type_name`),
  ADD KEY `subcategory_id` (`subcategory_id`);

ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `type_subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subcategories_id` (`subcategories_id`),
  ADD KEY `type_name` (`type_name`);


ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;



ALTER TABLE `products`
  ADD CONSTRAINT `products_fk0` FOREIGN KEY (`type_name`) REFERENCES `type` (`type_name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON UPDATE CASCADE;


ALTER TABLE `type_subcategories`
  ADD CONSTRAINT `type_subcategories_ibfk_1` FOREIGN KEY (`subcategories_id`) REFERENCES `subcategories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `type_subcategories_ibfk_2` FOREIGN KEY (`type_name`) REFERENCES `type` (`type_name`) ON DELETE SET NULL ON UPDATE CASCADE;