<?php
session_start();
require "template.php";

function get_content() { ?>

  <div class="container-fluid px-0">
    <div class="row no-gutters">
      <div class="col-md-6 about-page">
        <img class="about-img" src="assets/images/about.jpg">
      </div>
      <div class="col-md-5 about push-right" style="text-transform: lowercase; position: absolute; right: 30px;">
      Litterless is a one-stop shop of alternatives to single use products. At Litterless, switching to a waste-free life can be sinple and convenient! WE SOURCE OUR PRODUCTS FROM INDIVIDUALS AND BRANDS WITH MISSIONS TO CREATE POSITIVE ENVIRONMENTAL IMPACT. WE ARE PROUD TO CALL THEM OUR FRIENDS AND CONSTANT SOURCES OF INSPIRATION.
      </div>
    </div>
  </div>
  <?php } ?>