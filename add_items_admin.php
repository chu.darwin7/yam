<?php
session_start();
if(!isset($_SESSION['logged_in_user'])){
    header('location: index.php');
}

require "template.php";
function get_admin_content() {

require "controllers/connection.php";
?>

<div class="container-fluid">
    <div class="row">
        <div class="container">
            <h2 class="admin-dashboard" ">Admin</h2>

             <div class="col col-md-12">
                <div class="form-products">
                <form enctype="multipart/form-data" action="controllers/add_products.php" method="POST">

                    <div class="form-group">
                        <label for="productname">Product Name:</label>
                        <input class="form-control" type="text" name="productname">
                    </div>

                    <div class="form-group">
                        <label for="productsub">Product Subcategory:</label>
                        <select class="form-control" name="productsub">

                            <?php

                                $subcategoryQuery = "SELECT * FROM subcategories";
                                // global $conn;
                                $subcategories = mysqli_query($conn, $subcategoryQuery);

                                while($row = mysqli_fetch_assoc($subcategories)) { ?>
                                    <option value="<?php echo $row['id'] ?>"><?php echo $row['subcategory_name']?></option>
                                <?php } ?>

                        </select>
                    </div>


                    <div class="form-group">
                        <label for=producttype">Product Type:</label>
                        <select class="form-control" name="producttype">

                            <?php

                                require "controllers/connection.php";
                                $typeNameQuery = "SELECT * FROM type";
                                // global $conn;
                                $typeName = mysqli_query($conn, $typeNameQuery);

                                while($row = mysqli_fetch_assoc($typeName)) { ?>
                                    <option value="<?php echo $row['id'] ?>"><?php echo $row['type_name']?></option>
                                <?php } ?>

                        </select>
                    </div>

                  <div class="form-group">
                        <label for="productprice">Product Price:</label>
                        <input class="form-control" type="text" name="productprice">
                    </div>

                    <div class="form-group">
                        <label for="productdes">Product Description:</label>
                        <textarea class="form-control" rows="5" name="productdes"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="productimage">Product Image</label>
                        <input type="file" name="productimage" class="form-control-file" onchange="readURL(this)"; />
                         <br/>

                    </div>

                    <div class="form-group">
                        <button type="Submit" class="btn btn-success">Submit</button>
                    </div>

                    <div class="img-preview">
                        <img id="ShowImage" />
                     </div>
                </form>
            </div>

        </div>

</div>
</div>
</div>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ShowImage')
                    .attr('src', e.target.result)
                    // .width(100%)
                    // .height(100%);
                    // .width(150)
                    // .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>


<?php } ?>