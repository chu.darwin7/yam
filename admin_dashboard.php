<?php
session_start();
if(!isset($_SESSION['logged_in_user'])){
    header('location: index.php');
}

require "template.php";
function get_admin_content() {

require "controllers/connection.php";
?>

<div class="container-fluid">
	<div class="row">
		<div class="container">
			<h2 class="admin-dashboard" ">Admin</h2>

               <?php

                    require "controllers/connection.php";


                    $action = "controllers/add_products.php";
                    if(isset($_GET['id'])) {
                        require "controllers/connection.php";
                        $id = $_GET['id'];
                        $action = "controllers/edit_item.php?id=$id";
                        $update = "SELECT products.*,products.id as prodId,products.product_description as prodDes, type.type_name, subcategories.*, subcategories.id as subId FROM products LEFT JOIN type ON (type.id = products.type_id) LEFT JOIN subcategories ON (products.subcategory_id = subcategories.id) WHERE products.id = $id";
                        $updateItems = mysqli_query($conn, $update);
                        $result = mysqli_fetch_assoc($updateItems);
                        extract($result);

                    ?>

			 <div class="col col-md-12">
			    <div class="form-products">
				<form enctype="multipart/form-data" action="<?= $action ?>" method="POST">

				 	<div class="form-group">
						<label for="productname">Product Name:</label>
						<input class="form-control" type="text" value="<?= $result['product_name']?>" name="productname">
					</div>

					<div class="form-group">
						<label for="productsub">Product Subcategory:</label>
						<select class="form-control" value="<?= $result['subcategory_name']?>" name="productsub">

							<?php

								$subcategoryQuery = "SELECT * FROM subcategories";
								// global $conn;
								$subcategories = mysqli_query($conn, $subcategoryQuery);

								while($sub = mysqli_fetch_assoc($subcategories)) { ?>
									<option value="<?php echo $sub['id'] ?>" <?php if($row['subcategory_name']==$subcategory_name){echo "selected";}?>><?php echo $sub['subcategory_name']?></option>
								<?php } ?>

						</select>
					</div>


					<div class="form-group">
						<label for=producttype">Product Type:</label>
						<select class="form-control" name="producttype">

							<?php

								require "controllers/connection.php";
								$typeNameQuery = "SELECT * FROM type";
								// global $conn;
								$typeName = mysqli_query($conn, $typeNameQuery);

								while($type = mysqli_fetch_assoc($typeName)) { ?>
									<option value="<?php echo $type['id'] ?>"<?php if($type['type_name']==$type_name){echo "selected";}?>><?php echo $type['type_name']?></option>
								<?php } ?>

						</select>
					</div>

				  <div class="form-group">
						<label for="productprice">Product Price:</label>
						<input class="form-control" value="<?= $result['product_price']?>" type="text" name="productprice">
					</div>

					<div class="form-group">
						<label for="productdes">Product Description:</label>
						<textarea class="form-control" value="<?= $result['product_description']?>" rows="5" name="productdes"><?= $result['product_description']?></textarea>
					</div>

					<div class="form-group">
    					<label for="productimage">Product Image</label>
    					<input type="file" name="productimage" class="form-control-file" onchange="readURL(this)"; />
    					 <br/>

  					</div>

					<div class="form-group">
						<button type="Submit" class="btn btn-success">Submit</button>
					</div>

					<div class="img-preview">
    					<img src="<?= $result['product_image']?>" id="ShowImage" />
    			     </div>
				</form>
			</div>

		</div>
<?php } ?>

		<div class="col col-md-12">
			<div class="row">

				<div class="product_list">
					<div class="table-responsive">
					<table class="table">
					  <thead class="thead-light">
					    <tr>
					      <th scope="col">Product Name</th>
					      <th scope="col">Product Price</th>
					      <th scope="col">Product Description</th>
					      <th scope="col">Product Image</th>
					      <th scope="col">Product Subcategory</th>
                          <th scope="col">Product Type</th>
					      <th scope="col"></th>
					    </tr>
					  </thead>
					  <tbody>


					    	<?php

                    require "controllers/connection.php";
                    // $adminProducts = "SELECT * FROM products";
                    // $aList = mysqli_query($conn, $adminProducts);

                     $adminList = "SELECT products.*,products.id as prodId, type.type_name, subcategories.*, subcategories.id as subId FROM products LEFT JOIN type ON (type.id = products.type_id) LEFT JOIN subcategories ON (products.subcategory_id = subcategories.id)";
                     $aList = mysqli_query($conn, $adminList);

                    while($admin = mysqli_fetch_assoc($aList)) {
                        ?>
                    	<tr>
					      <th scope="row"><?php echo $admin['product_name'];?></th>
					      <td><?php echo $admin['product_price'];?></td>
					      <td><?php echo $admin['product_description'];?></td>
					      <td><?php echo $admin['product_image'];?></td>
					      <td><?php echo $admin['type_name'];?></td>
                          <td><?php echo $admin['subcategory_name'];?></td>
					      <td>
                            <div class="btn-group ml-auto mr-auto" role="group">
                                <a href="admin_product_page.php"><i class="fas fa-eye"></i></a>
                              <a href="controllers/edit_item.php?id=<?= $admin['prodId'];?>" name="edit"> <i class="fas fa-edit"></i></a>
                              <a href="controllers/delete_item.php?id=<?= $admin['prodId'];?>" name="delete"><i class="fas fa-trash-alt"> </i></a>
                            </div>
                           </td>
					    </tr>
					  <?php } ?>
					  </tbody>
					</table>
				</div>


			</div>
		</div>
	</div>
</div>
</div>
</div>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ShowImage')
                    .attr('src', e.target.result)
                    // .width(100%)
                    // .height(100%);
                    // .width(150)
                    // .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>


<?php } ?>