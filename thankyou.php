<?php
session_start();
require "template.php";
if(!isset($_SESSION['logged_in_user'])){
  header('location: ../login.php');
}

function get_content() { ?>

  <div class="container-fluid px-0">
    <div class="row no-gutters thankyou-page">
      <div class="col-md-12">
        <h1>Thank you for shopping at <br>
          <br>
          LITTERLESS!
        </h1>
      </div>
    </div>
  </div>

<?php

} ?>