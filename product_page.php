<?php

require "controllers/connection.php";

require "template.php";

function get_content() { ?>

<div class="container-fluid px-0">

  <span id="successMessage"></span>

    <div class="single-page-container">

      <div class="container">
        <div class="row no-gutters" id="for_remove">

          <?php
                    require "controllers/connection.php";
                    $filter = isset($_GET['id']) ? "WHERE id = ".$_GET['id'] : '';
                    $productItems = "SELECT * FROM products $filter";
                    $pItems = mysqli_query($conn, $productItems);

                    $result = mysqli_fetch_assoc($pItems);


        ?>

          <div class="col col-md-8">
            <img class="product-slides" src="<?= $result['product_image']?>">
          </div>


          <div class="col col-md-4 product-info">
            <div class="product-info-price items">
              <span><?= $result['product_price']?></span>
            </div>
            <div class="product-info-name items">
              <h3><?= $result['product_name']?></h3>
            </div>
            <div class="product-info-description items">
              <p><?= $result['product_description']?></p>
            </div>
            <div class="product-info-add items">
              <button onclick="addToCart(<?= $result['id'];?>)" type="button" class="btn btn-sm add-to-cart">
                <span>Add To Cart</span>
              </button>
            </div>
        </div>

      </div>

    </div>
</div id="content">

<div class="container">
  <div class="prod_contents_div">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a class="nav-link desc" href="#content" id="des">Description</a>
      </li>
      <li class="nav-item">
        <a class="nav-link mat" href="#content">Ingredients / Materials</a>
      </li>
      <li class="nav-item">
        <a class="nav-link care" href="#content">Care Instructions</a>
      </li>
      <li class="nav-item">
        <a class="nav-link end" href="#content">End of Life</a>
      </li>
    </ul>
    <div class="prod_contents">
    <p id="all"><?= $result['product_description']?></p>
  </div>
</div>
</div>




<script type="text/javascript">
function addToCart(itemid){
  let id = itemid;


  $.ajax({
    url:"controllers/add_to_cart.php",
    data: {"prod_id":id},
    method: "POST",
    async:false
    }).done(function(data) {
      $.alert({
          title: 'Successfully',
    content: 'added to cart!',
        });;
    });
}


$(document).ready(function () {
    // $('.add-to-cart').click( function() {
    //     const id = $(this).data('id');
    //     const quantity = $(this).prev().val();
    //     $(this).prev().val('');
    //     $.ajax({
    //         url : 'controllers/add_to_cart.php?id='+id,
    //         method : 'post',
    //         data : {quantity : quantity},
    //         success : data => {
    //             $('#cart-count').html(data);

    //             alert('added to cart');
    //         }
    //     });
    // });
});



$( ".desc" ).click(function() {
  $( "#all" ).toggle( "slow" );
});


</script>

<?php } ?>