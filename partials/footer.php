    <footer>
      <section class="section footer-top">
        <div class="container">
          <div class="row">
            <div class="col-md-5">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <div class="list-item-header">
                    About Us
                  </div>
                  <div class="list-group footer-list-group">
                    <p style="font-size: 12px; margin-right: 20px;">Litterless is a one-stop shop of alternatives to single use products. At Litterless, switching to a waste-free life can be sinple and convenient!</p>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6">
                  <div class="list-item-header">
                    Disclaimer
                  </div>
                  <div class="list-group footer-list-group">
                    <p style="font-size: 12px; margin-right: 20px;">This website is for educational purposes only.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-7">
              <div class="row">
                <div class="col-md-7">
                  <div class="list-item-header">
                    contact
                  </div>
                  <div class="contact-details">
                    <div class="media">
                      <div class="media-left contact-media-left">
                        <p>Address</p>
                      </div>
                      <div class="media-body">
                        <p>3rd Floor Caswynn Building, Timog Avenue, Quezon City</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="media-left contact-media-left">
                        <p>Mobile</p>
                      </div>
                      <div class="media-body">
                        <p>+440 231 258 12</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="media-left contact-media-left">
                        <p>Fax</p>
                      </div>
                      <div class="media-body">
                        <p>+440 231 258 12</p>
                      </div>
                    </div>
                    <div class="media">
                      <div class="media-left contact-media-left">
                        <p>Email</p>
                      </div>
                      <div class="media-body">
                        <p>litterless@gmail.com</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="list-item-header">
                    SOCIAL MEDIA
                  </div>
                   <div class="contact_socail transition">
                        <a href="facebook.com"><i class="fab fa-facebook-f img-circle"></i></a>
                        <a href="twitter.com"><i class="fab fa-twitter img-circle"></i></a>
                        <a href="instagram.com"><i class="fab fa-instagram img-circle"></i></a>
                        <a href="pinterest.com"><i class="fab fa-pinterest-p img-circle"></i></a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="footer-bottom">
        <div class="container">
          <hr class="footer-devider">
          <div class="row">
            <div class="col-md-5"><p class="copyright">Copyright © 2018 Miriam Tumpalan</p></div>
            <div class="col-md-5">
              <ul class="footer-menu">
                <li></li>
                 <li></li>
                 <li></li>
                 <li></li>
              </ul>
            </div>
            <div class="col-md-2">
              <a id="scroll-top-div" href="" class="pull-right back-top-btn">back to top <span class="btn-icon"><i class="fas fa-arrow-up"></i></span></a>
            </div>
          </div>
        </div>
      </section>
    </footer>