<?php
session_start();
if(isset($_SESSION['logged_in_user'])) {
    header('location: index.php');
}
require "controllers/connection.php";

require "template.php";

function get_content() { ?>

<div class="container-fluid px-0">

    <div class="container">

        <div class="row no-gutters">

            <div class="login-container">

                <div class="login-form">

                    <h4 style="text-align: center;">Login</h4>

                    <?php
                        if(isset($_SESSION['error_message'])) {
                        echo "<span class='error_message'>".$_SESSION['error_message']."</span>";
                        unset($_SESSION['error_message']);
                        }
                    ?>

                <form action="controllers/authenticate.php" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>

                    <button type="submit" class="btn btn-primary btn-submit">Login</button>

                    <a class="user-link" style="text-align: center;" href="register.php">Create account</a>
                </form>

                </div>

            </div>


        </div>

    </div>

</div>



<?php } ?>