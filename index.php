<?php
session_start();

require "template.php";

function get_content(){

require "controllers/connection.php";

?>

<div class="container-fluid px-0">

    <div class="main-header">
        <img class="main-header-img" src="images/banner-bg.jpg" alt="home">
        <ul class="home-social">
      <!--       <li>
                <a href="#0"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
            </li>
            <li>
                <a href="#0"><i class="fab fa-twitter"></i><span>Twitter</span></a>
            </li>
            <li>
                <a href="#0"><i class="fab fa-instagram"></i><span>Instagram</span></a>
            </li>
            <li>
                <a href="#0"><i class="fab fa-pinterest-p"></i><span>Pinterest</span></a>
            </li>
        </ul> -->
    </div>
</div>


<!-- Categories -->
<div class="second-page">
    <div class="container-fluid px-0">
        <div class="row mx-0">
            <section class="box-1">
                <h4 class="line-sub my-5">Welcome to <span class="sub-text"> Litterless!</span></h4>
                <div class="container-fluid my-5 px-0">
                    <div class="row no-gutters">
                      <!--   <div class="col-md-3">
                            <div class="inner-box">
                                <img class="img-fluid header-slides" src="images/box2-landing.jpg">
                                <img class="img-fluid header-slides" src="images/living-room.jpg">
                                <img class="img-fluid header-slides" src="images/bathroom.jpg">
                                <img class="img-fluid header-slides" src="images/travel.jpg">
                                <img class="img-fluid header-slides" src="images/bathroom.jpg">
                            </div>
                        </div> -->
                        <div class="col-md-3">
                            <div class="inner-box">
                                <img class="img-fluid" src="images/living-room.jpg">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="inner-box">
                                <img class="img-fluid" src="images/box2-landing.jpg">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="inner-box">
                                <img class="img-fluid" src="images/bathroom.jpg">
                            </div>
                        </div>


                         <div class="col-md-3">
                            <div class="inner-box">
                                <img class="img-fluid" src="images/travel.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- <section class="box-2">
                <div class="container my-5">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <div class="inner-box2"></div>
                        </div>
                    </div>
                </div>
            </section> -->
        </div>
    </div>
</div>

<div class="third-page">
    <div class="container-fluid my-5 px-0">
        <div class="row no-gutters">
            <div class="col-md-4 left-third-container">
                <img src="images/straw.jpg">
            </div>
            <div class="col-md-4 middle-third-container">
                <img src="images/text1.png">
            </div>
            <div class="col-md-4 right-third-container">
                <img src="images/blank_boxes.png" style="position: absolute;right: 0;">
            </div>

        </div>
    </div>
</div>

<div class="blog">
    <div class="container-fluid px-0">
        <div class="row no-gutters">
            <div class="col-lg-12 box-2">
                <h4 class="line-sub my-5">LitterLess<span class="sub-text"></span></h4>
                <div class="container">
                    <div class="col col-md-4 blog-box1">
                        <img class="img-fluid gif"  src="assets/images/shaving.gif">
                    </div>
                    <div class="col col-md-4 blog-box1">
                         <img class="img-fluid gif2"  src="assets/images/wrap.gif">
                    </div>
                    <div class="col col-md-4 blog-box1">
                         <img class="img-fluid gif3"  src="assets/images/water.gif">
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>



<?php

}

?>