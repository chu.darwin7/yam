<?php
session_start();
if(isset($_SESSION['logged_in_user'])) {
    header('location: index.php');
}
require "controllers/connection.php";

require "template.php";

function get_content() { ?>

    <div class="container-fluid px-0">

    <div class="container">

        <div class="row no-gutters">

            <div class="register-container">

                <div class="register-form">

                    <h4 style="text-align: center;">Register</h4>

                <form id="register_form" action="controllers/register_endpoint.php" method="POST">
                      <div class="form-group">
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Complete Name"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control" id="number" name="number" placeholder="Contact Number"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="eadd" name="eadd" placeholder="Email Address"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password"><span></span>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password"><span></span>
                    </div>

                    <button type="button" id="registerBtn" class="btn btn-primary btn-submit reg">Register Acount</button>
                </form>

                </div>

            </div>


        </div>

    </div>

</div>

<script type="text/javascript">
$('#registerBtn').click( () => {
    let errorFlag = false;
    const username = $('#username').val();

    if(username.length == 0) { //username field is empty
        $('#username').next().css('color','red');
        $('#username').next().html('this field is required');
        errorFlag = true;
    } else {
        $.ajax({
            url : 'controllers/check_username.php',
            method : 'post',
            data : {username : username},
            async : false
        }).done( data => {
            if(data == 'meron') {
                $('#username').next().css('color','red');
                $('#username').next().html('username is already taken');
                errorFlag = true;
            } else {
                $('#username').next().css('color','green');
                $('#username').next().html('username is available');
            }
        });
    }

    const name = $('#fullname').val();
    if(name.length == 0) { //checks if password field is empty
        $('#fullname').next().css('color','red');
        $('#fullname').next().html('this field is required');
        errorFlag = true;
    }

    const add = $('#address').val();
    if(add.length == 0) { //checks if password field is empty
        $('#address').next().css('color','red');
        $('#address').next().html('this field is required');
        errorFlag = true;
    }

    const eadd = $('#eadd').val();
    if(eadd.length == 0) { //checks if password field is empty
        $('#eadd').next().css('color','red');
        $('#eadd').next().html('this field is required');
        errorFlag = true;
    }

    const password = $('#password').val();
    const confirmPassword = $('#confirm_password').val();
    if(password.length == 0) { //checks if password field is empty
        $('#password').next().css('color','red');
        $('#password').next().html('this field is required');
        errorFlag = true;
    } else {
        $('#password').next().html(''); //removes the error message
        if(password !== confirmPassword) {
            $('#confirm_password').next().css('color','red');
            $('#confirm_password').next().html('passwords did not match');
            errorFlag = true;
        } else {
            $('#confirm_password').next().html('');
        }
    }

    if(errorFlag == false) {
        $('#register_form').submit();
    }
});
</script>

<?php } ?>