<?php

require "template.php";

function get_content() {

require "controllers/connection.php";
?>

<div class="container">
    <div class="row">
      <span id="successMessage"></span>
            <h2 class="page-header mx-auto py-5">Shopping Cart</h2>
                <div class="col col-md-12">
                    <div class="row">
                            <div class="table-responsive">
                                <table class="table cart-table"id="for_remove">
                                    <thead class="cart-table-header">
                                        <tr>
                                          <th scope="col">Item</th>
                                          <th scope="col">Name</th>
                                          <th scope="col">Quantity</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Total Price</th>
                                          <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php

                                        require "controllers/connection.php";
                                        // $adminProducts = "SELECT * FROM products";
                                        // $aList = mysqli_query($conn, $adminProducts);
                                        // var_dump($_SESSION['cart']);
                                         $subtotal = 0;

                                  if(!isset($_SESSION['cart']) || count($_SESSION['cart']) == 0){
                                    echo "<h2 class='text-center'>No items in cart.</h2>";

                                  } else {
                                    $index = 0;
                                    ?>

                                   <!--  // var_dump($_SESSION['cart']); -->


                         <?php
                                      foreach($_SESSION['cart'] as $prod){
                                        $id = $prod['id'];

                                        $cartQuery = "SELECT products.*, type.type_name, subcategories.* FROM products LEFT JOIN type ON (type.id = products.type_id) LEFT JOIN subcategories ON (products.subcategory_id = subcategories.id) WHERE products.id = $id";
                                         $cartList = mysqli_query($conn, $cartQuery);
                                         // echo $cartQuery;
                                         // var_dump($conn);?

                                        $row = mysqli_fetch_assoc($cartList); {

                                          $total_price = $row['product_price']*$prod['quantity'];
                                          $subtotal += $total_price;

                                        } ?>

                                        <tr>
                                          <th scope="row" class="item-preview">
                                              <img id="ShowItem" src="<?php echo $row['product_image'];?>" style="width: 80%;"/>
                                          </th>
                                           <td><?php echo $row['product_name'];?></td>
                                          <td>

                                            <div class="qty" style="width: 100px;">
                                              <form action="controllers/update_cart.php" method="POST">
                                              <button type="button" class="qty-minus" value="<?php echo $id?>">-</button>
                                              <input id="changequantity<?php echo $id?>" min=1 class="form-control form-control-sm w-50" type="text" disabled value="<?= $prod['quantity'];?>" name="updatecart" style="text-align: center; display: inline; background: transparent;">
                                              <button type="button" class="qty-add" value="<?php echo $id?>">+</button>
                                            </form>
                                            </div>
                                          </td>
                                          <td id="itemPrice<?= $id?>"><?php echo $row['product_price'];?></td>
                                          <td class="price-bottom" id="totalPrice<?= $id?>"><?php echo $total_price;?></td>
                                           <td><a href="controllers/delete_cart_item.php?index=<?= $index?>"><i class="fas fa-times qty-delete"></i></a></td>
                                        </tr><?php
                                      $index++;
                                      }
                                  }

                                  ?>

                                    </tbody>
                                </table>
                                <div class="amountToBePaid">
                                  <div >Total: Php<span class="mx-3"id="displayPrice"><?php echo $subtotal ?></span></div>
                                </div>
                                <?php
                                 if(isset($_SESSION['cart'])) { ?>
                                   <a href="controllers/empty_cart.php"><button class="btn btn-outline-secondary my-3" type="button">Empty Cart</button></a>
                                    <a href="checkout.php"><button class="btn btn-outline-secondary my-3" type="button">Proceed To Checkout</button></a>
                               <?php } else { ?>
                                  <?php }?>
                    </div>
                </div>
        </div>
    </div>

</div>
</div>
</div>

<script type="text/javascript">
  let ops="";
  let all="";
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ShowItem')
                    .attr('src', e.target.result)
                    // .width(20%);
                    // .height(30%);
                    // .width(150)
                    // .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }


    //   function changequant(itemid){
    //   let id = itemid;
    //   let item_quantity = $('#changequantity'+id).attr("value");
    //     // alert(item_quantity);

    //   $.ajax({
    //     url: 'controllers/update_cart.php',
    //     type: 'POST',
    //     data: {id: id,ops:ops, item_quantity:item_quantity},async:false
    //   })
    //   .done(function(data) {
    //     let iQuantity = $('#itemPrice'+id).html();
    //     let iPrice = $('#changequantity'+id).attr("value");

    //    //  // console.log(eval($('#itemPrice'+id).html()+$('#changequantity'+id).html()));
    //    $('#totalPrice'+id).html(parseInt(iQuantity)*parseInt(iPrice));
    //   });
    // }


          function changequant(itemid){
      let id = itemid;
      let item_quantity = $('#changequantity'+id).attr("value");
        // alert(item_quantity);

      $.ajax({
        url: 'controllers/update_cart.php',
        type: 'POST',
        data: {id: id,ops:ops, item_quantity:item_quantity},async:false
      })
      .done(function(data) {
        let iQuantity = $('#itemPrice'+id).html();
        let iPrice = $('#changequantity'+id).attr("value");

       //  // console.log(eval($('#itemPrice'+id).html()+$('#changequantity'+id).html()));
       $('#totalPrice'+id).html(parseInt(iQuantity)*parseInt(iPrice));
      });
    }


$('#for_remove').on('click','.qty-add', function(){
  let total = $('#displayPrice').html();
  let value = $(this).attr("value");
  // console.log(value);
  let quantity = parseInt($("#changequantity"+value).attr("value"),10);

  // console.log(parseInt($("#changequantity"+value).attr("value"),10));
  $("#changequantity"+value).attr("value",quantity+1);
  ops="+";
  changequant(value);
  let iPrice = parseInt($('#itemPrice'+value).html());
  let iTotal = parseInt($('#displayPrice').html());

  $('#displayPrice').html(eval(total+'+'+iPrice));
  // console.log("iTotal="+iTotal+" price= "+iPrice)
  // $('#displayPrice').html(iTotal+iPrice);

});

$('#for_remove').on('click','.qty-minus', function(){
  let total = $('#displayPrice').html();
  let value = $(this).attr("value");
  let quantity = parseInt($("#changequantity"+value).attr("value"));

if(quantity == 1){
  $.alert({
          title: 'Not Allowed',
    content: 'Minimum of one item only.',
        });
} else {
  $("#changequantity"+value).attr("value",quantity-1);
  ops="-";
  changequant(value);

  let iPrice = parseInt($('#itemPrice'+value).html());
  let iTotal = parseInt($('#displayPrice').html());

  $('#displayPrice').html(eval(total+'-'+iPrice));


}


  // console.log("iTotal="+iTotal+" price= "+iPrice)
  // $('#displayPrice').html(iTotal-iPrice);

});

</script>

<?php

 } ?>