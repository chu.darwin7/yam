<?php
session_start();
if(!isset($_SESSION['logged_in_user'])){
    // header('location: login.php');
   header('location: login.php');
}
require "template.php";


function get_content() { ?>

    <div class="container-fluid px-0">
        <div class="row no-gutters checkout-page">
            <div class="col-md-6 checkout-table">
                <table class="table ">
                  <thead class="thead-dark">
                      <th colspan="6" style="text-align: center;">Order Details</th>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Name</th>
                      <th scope="row">Qty</th>
                      <th scope="row">Total</th>
                  </tr>

                      <?php
                      require "controllers/connection.php";
                      $subtotal = 0;
                      foreach($_SESSION['cart'] as $orderdetail){
                        extract($orderdetail);


                        $orderQuery = "SELECT products.product_name, products.product_price FROM products WHERE products.id = $id";
                        $orderList = mysqli_fetch_assoc(mysqli_query($conn, $orderQuery));

                        $total_price = $orderList['product_price']*$quantity;
                        $subtotal += $total_price;
                        ?>
                        <tr>
                          <td><?= $orderList['product_name']?></td>
                          <td><?= $quantity ?></td>
                          <td><?=$total_price?></td>
                          <!-- <input type="hidden" name="totalprice" value="<?=$total_price?>"> -->
                          <td></td>
                      </tr>

                  <?php } ?>
          </tbody>
      </table>
      <table class="table ">
          <th colspan="6" style="text-align: right; padding-right:110px; padding-top: 20px;">Subtotal: Php <?=$subtotal?></th>
      </table>
  </div>

  <?php
  require "controllers/connection.php";
  $name = $_SESSION['logged_in_user'];
  $user_checkout = "SELECT u.id, us.* FROM users u JOIN user_details us ON (u.id=us.user_id) WHERE u.username='$name'";
  $result = mysqli_query($conn, $user_checkout);

                // echo $user['full_name']." ".$user['id'];

  while ($user = mysqli_fetch_assoc($result)) { ?>

    <div class="col-md-5 checkout-table right">

     <table class="table ">
      <thead class="thead-dark">
          <th colspan="6" style="text-align: center;">Delivery Details</th>
      </thead>
      <tbody>
        <form action="controllers/thankyou_endpoint.php" method="POST">
            <tr>
              <th scope="row">Customer</th>
              <th colspan="2">Address</th>
              <th scope="row">Email</th>
              <th scope="row">Contact Number</th>
          </tr>
          <tr>
              <td><?= $user['full_name']?></td>
              <td colspan="2"><?= $user['address']?></td>
              <input type="hidden" name="address" value="<?= $user['address']?>">
              <td><?= $user['email_address']?></td>
              <td><?= $user['contact_number']?></td>
              <input type="hidden" name="telnumber" value="<?= $user['contact_number']?>">
          </tr>
      </tbody>
  </table>

        <div class="thankyou">
          <div>
            <button id="ty" class="btn btn-secondary">Thank you</button></a>
        </div>
    </div>
        </form>
</div>
<?php

// var_dump($_SESSION['cart']);

} ?>

</div>
</div>


<script type="text/javascript">

    $('#ty').click( function() {
        $.confirm({
    title: 'Ready to purchase these items?',
    buttons: {
        confirm: function () {
            $.alert('Confirmed!');
        },
        cancel: function () {
            $.alert('Canceled!');
        },
        }
    })
});






</script>

<?php } ?>