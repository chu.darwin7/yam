<div class="panel-group accordion-panel" id="accordion">
    <div class="panel panel-default">
        <h5>Categories</h5>
        <hr>
        <?php

            require "controllers/connection.php";
            $sql = "SELECT * FROM categories";
            $categories = mysqli_query($conn, $sql);

            while($row = mysqli_fetch_assoc($categories)) { ?>

        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row['id'] ?>" style="text-decoration: none;"><span class="glyphicon glyphicon-th"></span><?= $row['category_name'] ?></a>
            </h4>
        </div>
        <div id="collapse<?php echo $row['id'] ?>" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">

                    <?php

                        $cat_id=$row['id'];

                        $sql = "SELECT subcategories.* FROM `subcategories` JOIN categories ON (subcategories.category_id = categories.id) WHERE categories.id = $cat_id";
                        $subcategories = mysqli_query($conn, $sql);
                        // print_r($subcategories);


                        while($cat = mysqli_fetch_assoc($subcategories)) {
                           ?>
                    <tr>
                        <td>
                            <a href="shop.php?sub=<?php echo $cat['id'] ?>"><?php echo $cat['subcategory_name'];?></a>
                        </td>
                    </tr>
                       <?php } ?>
                </table>
            </div>
        </div>
    <?php } ?>
    </div>
</div>

<br>
<div class="panel-group accordion-panel" id="accordion">
    <div class="panel panel-default">
        <h5>Sort By</h5>
        <hr>
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSort" style="text-decoration: none;"><span class="glyphicon glyphicon-th"></span>Price</a>
            </h4>
        </div>
        <div id="collapseSort" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <a href="controllers/sort.php?prod=asc#shop-div">Lowest to Highest</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a href="controllers/sort.php?prod=desc#shop-div">Highest to Lowest</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>