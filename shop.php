<?php
require "template.php";

function get_content() {

?>

<div class="container-fluid px-0">

    <div class="main-header">
        <img class="main-header-img" src="images/banner-bg.jpg" alt="home">
    </div>
</div>



    <div class="container-fluid shop-container" id="shop-div" data-spy="scroll">


        <div class="row no-gutters">

            <div class="col-md-2 side-nav-shop">

                <?php require 'partials/accordion.php'; ?>

            </div>

            <div class="col col-md-8 shop">
                <div class="row">

                    <?php

                    require "controllers/connection.php";
                    $filter = isset($_GET['sub']) ? "WHERE subcategory_id = ".$_GET['sub'] : '';
                    $filter .= isset($_SESSION['sort']) ? $_SESSION['sort'] : '' ;
                    $productItems = "SELECT * FROM products $filter";
                    $pItems = mysqli_query($conn, $productItems);



                    while($row = mysqli_fetch_assoc($pItems)) {


                        ?>

                    <div class="col-md-3 mx-4 my-5" onclick="location.href='product_page.php?id=<?= $row['id'];?>'">
                        <div class="card-group" style="width: 15rem; text-align: center;">
                            <div class="card border-0 rounded-0">
                                <img class="card-img-top border-0 img-fluid" src="<?php echo $row['product_image'];?>" alt="">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $row['product_name'];?></h5>
                                </div>
                                <div class="card-footer border-0">
                                    <small><?php echo $row['product_price'];?></small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>



    </div> <!-- end of ROW CONTAINER -->

</div> <!-- end of CONTAINER FLUID -->


<?php } ?>