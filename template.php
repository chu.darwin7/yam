<?php if (session_status() == PHP_SESSION_NONE) session_start(); ?>

<!DOCTYPE html>
<!--[if lt IE 9 ]><html class="no-js oldie" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>

    <?php require_once 'partials/header.php';?>

</head>
<body id="top">

    <?php require_once 'partials/nav.php';?>

    <?php
    if (isset($_SESSION['logged_in_user'])) {
        if(isset($_SESSION['role']) && $_SESSION['role']=='admin'){get_admin_content();}
    else {
        get_content();
    }
    } else {
        get_content();
    }



     ?>

    <?php require_once 'partials/footer.php';?>

</body>
</html>