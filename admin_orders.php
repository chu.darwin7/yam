<?php
session_start();
if(!isset($_SESSION['logged_in_user'])){
    header('location: index.php');
}

require "template.php";
function get_admin_content() {

require "controllers/connection.php";
?>

<div class="container-fluid">
    <div class="row">
        <div class="container">
            <h2 class="admin-dashboard">Orders List</h2>

               <?php

                    require "controllers/connection.php";


                    $action = "controllers/add_products.php";

                    if(isset($_GET['id'])) {
                        require "controllers/connection.php";
                        $id = $_GET['id'];
                        $action = "controllers/edit_item.php?id=$id";
                        $update = "SELECT products.*,products.id as prodId,products.product_description as prodDes, type.type_name, subcategories.*, subcategories.id as subId FROM products LEFT JOIN type ON (type.id = products.type_id) LEFT JOIN subcategories ON (products.subcategory_id = subcategories.id) WHERE products.id = $id";
                        $updateItems = mysqli_query($conn, $update);
                        $result = mysqli_fetch_assoc($updateItems);
                        extract($result);

                    } ?>


        <div class="col col-md-12">
            <div class="row">

                <div class="product_list">
                    <div class="table-responsive">
                    <table class="table">
                      <thead class="thead-light">
                        <tr>
                          <th width="15%">Transaction Code:</th>
                          <th width="15%">Customer Name:</th>
                          <th cwidth="30%">Delivery Address:</th>
                          <th width="15%">Contact Number:</th>
                          <th width="10%">Payment Method:</th>
                          <th width="10%">Status:</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>


                            <?php

                    require "controllers/connection.php";
                    // $adminProducts = "SELECT * FROM products";
                    // $aList = mysqli_query($conn, $adminProducts);

                     $orderList = "SELECT orders.* as o ,orders_details.* as   "

                     "SELECT products.*,products.id as prodId, type.type_name, subcategories.*, subcategories.id as subId FROM products LEFT JOIN type ON (type.id = products.type_id) LEFT JOIN subcategories ON (products.subcategory_id = subcategories.id)";

                     $oList = mysqli_query($conn, $orderList);

                    while($orders = mysqli_fetch_assoc($oList)) {
                        ?>
                        <tr>
                          <th scope="row"><?php echo $admin['product_name'];?></th>
                          <td><?php echo $admin['product_price'];?></td>
                          <td><?php echo $admin['product_description'];?></td>
                          <td><?php echo $admin['product_image'];?></td>
                          <td><?php echo $admin['type_name'];?></td>
                          <td><?php echo $admin['subcategory_name'];?></td>
                          <td>
                            <div class="btn-group ml-auto mr-auto" role="group">
                                <a href=""><i class="fas fa-eye"></i></a>
                              <a href="controllers/edit_item.php?id=<?= $admin['prodId'];?>" name="edit"> <i class="fas fa-edit"></i></a>
                              <a href="controllers/delete_item.php?id=<?= $admin['prodId'];?>" name="delete"><i class="fas fa-trash-alt"> </i></a>
                            </div>
                           </td>
                        </tr>
                      <?php } ?>
                      </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>


<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#ShowImage')
                    .attr('src', e.target.result)
                    // .width(100%)
                    // .height(100%);
                    // .width(150)
                    // .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>


<?php } ?>