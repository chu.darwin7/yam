<?php
session_start();
require "template.php";
if(!isset($_SESSION['logged_in_user'])){
    header('location: ../login.php');
}

function get_admin_content() { ?>

    <div class="container-fluid px-0">
        <div class="row no-gutters checkout-page">
            <div class="col-md-12 checkout-table">
                <table class="table ">
                  <thead class="thead-dark">
                      <th colspan="6" style="text-align: center;">Order Details</th>
                  </thead>
                  <tbody>
                    <tr>
                      <th scope="row">Customer Name</th>
                      <th scope="row">Item</th>
                      <th scope="row">Quantity</th>
                  </tr>

                      <?php
                      require "controllers/connection.php";

                        $viewQuery = "SELECT orders.*, order_details.* FROM products JOIN orders ON (orders.id = order_details.orders_id)";
                        $viewList = mysqli_query($conn, $viewQuery);

                         while ( $viewResult = mysqli_fetch_assoc($viewList)) { ?>

                        ?>
                        <tr>
                          <td><?= $orderList['product_name']?></td>
                          <td><?= $quantity ?></td>
                          <td><?=$total_price?></td>
                          <!-- <input type="hidden" name="totalprice" value="<?=$total_price?>"> -->
                          <td></td>
                      </tr>

                  <?php } ?>
          </tbody>
      </table>
      <table class="table ">
          <th colspan="6" style="text-align: right; padding-right:110px; padding-top: 20px;">Subtotal: Php <?=$subtotal?></th>
      </table>
  </div>

  <?php
  require "controllers/connection.php";
  $name = $_SESSION['logged_in_user'];
  $user_checkout = "SELECT u.id, us.* FROM users u JOIN user_details us ON (u.id=us.user_id) WHERE u.username='$name'";
  $result = mysqli_query($conn, $user_checkout);

                // echo $user['full_name']." ".$user['id'];

  while ($user = mysqli_fetch_assoc($result)) { ?>

    <div class="col-md-5 checkout-table right">

     <table class="table ">
      <thead class="thead-dark">
          <th colspan="6" style="text-align: center;">Delivery Details</th>
      </thead>
      <tbody>
        <form action="controllers/thankyou_endpoint.php" method="POST">
            <tr>
              <th scope="row">Customer</th>
              <th colspan="2">Address</th>
              <th scope="row">Email</th>
              <th scope="row">Contact Number</th>
          </tr>
          <tr>
              <td><?= $user['full_name']?></td>
              <td colspan="2"><?= $user['address']?></td>
              <input type="hidden" name="address" value="<?= $user['address']?>">
              <td><?= $user['email_address']?></td>
              <td><?= $user['contact_number']?></td>
              <input type="hidden" name="telnumber" value="<?= $user['contact_number']?>">
          </tr>
      </tbody>
  </table>

        <div class="thankyou">
          <div>
            <button class="btn btn-secondary">Thank you</button></a>
        </div>
    </div>
        </form>
</div>
<?php } ?>

</div>
</div>
<?php } ?>